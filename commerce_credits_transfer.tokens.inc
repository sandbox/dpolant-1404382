<?php
/*
 * Implements hook_tokens_info().
 */
function commerce_credits_transfer_tokens_info() {
  // Token for number of credits being transferred, usable from within rules
  $info['credits_transferred']['number'] = array(
    'name' => t('Credits transferred'),
    'description' => t('Number of credits transferred during a credits-transfer event')
  );
  
  return $info;
}

/*
 * Implements hook_tokens().
 */
function commerce_credits_transfer_tokens($type, $tokens, array $data = array(), array $options = array()) {
  if ($type == 'token') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'number':
          $replacements[$original] = $data['token'];
          
          break;
      }
    }
  }
  if (isset($replacements)) {
    return $replacements;
  }
}
