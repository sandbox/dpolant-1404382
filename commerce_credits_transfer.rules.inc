<?php
/*
 * Implements hook_rules_event_info().
 */
function commerce_credits_transfer_rules_event_info() {
  $items = array();
  $items['commerce_credits_transfer_successful_user_transfer'] = array(
    'label' => t('When a successful user-to-user transfer of credits has taken place'),
    'variables' => array(
      'user_from' => array(
        'type' => 'user',
        'label' => t('Transfer from'),
        'description' => t('User to transfer from'),
      ),
      'user_to' => array(
        'type' => 'user',
        'label' => t('Transfer to'),
        'description' => t('User to transfer to'),
      ),
      'credit_type' => array(
        'type' => 'commerce_credits_type',
        'label' => t('Credit type'),
        'description' => t('Credit type to be transfered.'),
        'options list' => 'commerce_credits_types_list',
      ),
      'credits_transferred' => array(
        'type' => 'token',
        'label' => t('Number of credits'),
        'description' => t('Number of credits that should be transfered.'),
      )
    ),
    'group' => t('Commerce Credits Transfer'),
  );    
  $items['commerce_credits_transfer_successful_transfer'] = array(
    'label' => t('When a successful transfer of credits has taken place'),
    'variables' => array(
      'entity_from' => array(
        'type' => 'entity',
        'label' => t('Transfer from'),
        'description' => t('Entity to transfer from'),
      ),
      'entity_to' => array(
        'type' => 'entity',
        'label' => t('Transfer to'),
        'description' => t('Entity to transfer to'),
      ),
      'credit_type' => array(
        'type' => 'commerce_credits_type',
        'label' => t('Credit type'),
        'description' => t('Credit type to be transfered.'),
        'options list' => 'commerce_credits_types_list',
      ),
      'credits_transferred' => array(
        'type' => 'token',
        'label' => t('Number of credits'),
        'description' => t('Number of credits that should be transfered.'),
      )
    ),
    'group' => t('Commerce Credits Transfer'),
  );   
  return $items;
}

/*
 * Implements hook_rules_action_info().
 */
function commerce_credits_transfer_rules_action_info() {
  $items = array();
  $items['commerce_credits_transfer_transfer_credits'] = array(
    'label' => t('Transfer credits'),
    'group' => t('Commerce Credits Transfer'),
    'parameter' => array(
      'entity_from' => array(
        'type' => 'entity',
        'label' => t('Transfer from'),
        'description' => t('Entity to transfer from'),
        'wrapped' => TRUE,
      ),
      'entity_to' => array(
        'type' => 'entity',
        'label' => t('Transfer to'),
        'description' => t('Entity to transfer to'),
        'wrapped' => TRUE,
      ),
      'credit_type' => array(
        'type' => 'text',
        'label' => t('Credit type'),
        'description' => t('Credit type to be transfered.'),
        'options list' => 'commerce_credits_types_list',
      ),
      'number' => array(
        'type' => 'integer',
        'label' => t('Number of credits'),
        'description' => t('Number of credits that should be transfered.'),
      )
    ),
    'callbacks' => array(
      'execute' => 'commerce_credits_transfer_rules_action_transfer_credits'
    )
  );
  return $items;
}


/**
 * Transfers credits from one entity to another
 */
function commerce_credits_transfer_rules_action_transfer_credits(EntityDrupalWrapper $from, EntityDrupalWrapper $to, $type, $number) {
  commerce_credits_transfer_credit_transfer_by_type($from, $to, $type, $number);
}