<?php
/**
 * Add to an array of errors for a credits transfer before it is executed in 
 * order to halt the transfer.
 * 
 * @param EntityDrupalWrapper $from_entity
 *  Entity that credits are being transferred from
 * 
 * @param EntityDrupalWrapper $to_entity
 *  Entity that credits are being transferred to
 * 
 * @param type $credit_type
 *  Type of credits being transferred
 * 
 * @param type $credits 
 *  Number of credits being transferred
 * 
 * @return array
 *  List of errors thrown
 */
function hook_commerce_credits_transfer_deny_transfer(EntityDrupalWrapper $from_entity, EntityDrupalWrapper $to_entity, $credit_type, $credits) {
  $errors = array();
  
  // Deny transfer if target is not a user
  if ($to_entity->type() != 'user') {
    // Must escape
    $errors[] = t('Transferred denied for entity because it is a @type rather than a user', array('@type' => $to_entity->type()));
  }
  return $errors;
}